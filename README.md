[![pipeline status](https://gitlab.com/mvysny/jdbi-orm-vaadin/badges/master/pipeline.svg)](https://gitlab.com/mvysny/jdbi-orm-vaadin/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.gitlab.mvysny.jdbiormvaadin/jdbi-orm-vaadin/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.gitlab.mvysny.jdbiormvaadin/jdbi-orm-vaadin)

# JDBI-ORM: Vaadin Support

Adds support for [Vaadin](https://vaadin.com/) for the [jdbi-orm](https://gitlab.com/mvysny/jdbi-orm).
Requires Java 17+. Tested with Vaadin 24, might work with Vaadin 23.

## Quickstart

Add the following lines to your Gradle script, to include this library in your project:
```groovy
repositories {
    mavenCentral()
}
dependencies {
    compile("com.gitlab.mvysny.jdbiormvaadin:jdbi-orm-vaadin:x.y")
}
```

> Note: obtain the newest version from the tag name at the top of the page

Maven: (it's very simple since jdbi-orm-vaadin is in Maven Central):

```xml
<project>
	<dependencies>
		<dependency>
			<groupId>com.gitlab.mvysny.jdbiormvaadin</groupId>
			<artifactId>jdbi-orm-vaadin</artifactId>
			<version>x.y</version>
		</dependency>
    </dependencies>
</project>
```

To set the data provider to your Grid:

```java
final EntityDataProvider<Person> dp = new EntityDataProvider<>(Person.class);
// optionally set an unremovable filter, to always filter the records.
dp.setFilter(Person.AGE.ge(18));
grid.setDataProvider(dp);
```

To set the data provider to your ComboBox:

```java
final EntityDataProvider<Person> dp = new EntityDataProvider<>(Person.class);
// optionally set an unremovable filter, to always filter the records.
dp.setFilter(Person.AGE.ge(18));
comboBox.setDataProvider(dp.withStringFilter(filter -> Person.NAME.likeIgnoreCase(filter + "%")));
```

## Grid Sorting

You need to set the Grid Column key to a JDBI-ORM Property identifier; that way we can reconstruct
the Property back from Vaadin's QuerySortOrder:

```java
final Grid.Column<Person> idColumn = personGrid.addColumn(Person::getId)
        .setHeader("ID")
        .setSortable(true)
        .setKey(Person.ID.toExternalString());
```

## Grid Filters

One way of adding filters to your grid is to add a Grid header bar just for filter components,
then add filter components as cells to the header bar:

```java
// append first header row: the column captions and the sorting indicator will appear here.
personGrid.appendHeaderRow();
// the second header row will host filter components.
final HeaderRow filterBar = personGrid.appendHeaderRow();

final FilterTextField nameFilter = new FilterTextField();
final Grid.Column<Person> nameColumn = personGrid.addColumn(Person::getName)
        .setHeader("Name")
        .setSortable(true)
        .setKey(Person.NAME.toExternalString());
filterBar.getCell(nameColumn).setComponent(nameFilter);
nameFilter.addValueChangeListener(e -> updateFilter());
```

Then, when any of the filter component
changes, you need to calculate the jdbi-orm `Condition` from the values of all filters as follows:

```java
private void updateFilter() {
    Condition c = Condition.NO_CONDITION;
    if (!nameFilter.getValue().isBlank()) {
        c = c.and(Person.NAME.likeIgnoreCase(nameFilter.getValue().trim() + "%"));
    }
    if (!isActiveFilter.isEmpty()) {
        c = c.and(Person.ACTIVE.eq(isActiveFilter.getValue()));
    }
    dataProvider.setFilter(c);
}
```

Alternatively, you might have a `FilterBean` populated by the dialog. Whenever the "Apply" button
of the dialog is pressed, you populate the FilterBean from the components; you can then
calculate the `Condition` from the bean in a similar way as above.

See the [jdbi-orm-vaadin-crud-demo](https://github.com/mvysny/jdbi-orm-vaadin-crud-demo) example
project for more details.

This project offers additional filter components:

* `BooleanFilterField`: allows the user to select `true` or `false` or clear the selection and disable this filter.
* `EnumFilterField`: allows the user to select one or more enum constants. If all constants or no constant is selected, the filter is disabled.
* `FilterTextField`: a simple TextField filter. Usually matched with a column using the `likeIgnoreCase()` operator.
* `DateRangePopup`: allows the user to select a date range. The range may be open (only the 'from' or 'to' date filled in, but not both). Usually matched using the `between()` operator.
* `NumberRangePopup`: allows the user to select a numeric range. The range may be open (only the 'from' or 'to' number filled in, but not both). Usually matched using the `between()` operator.

The following example shows the creation of the `Condition` from the filter components. The example is taken from the
[jdbi-orm-vaadin-crud-demo](https://github.com/mvysny/jdbi-orm-vaadin-crud-demo) example app.

```java
private final NumberRangePopup idFilter = new NumberRangePopup();
private final FilterTextField nameFilter = new FilterTextField();
private final NumberRangePopup ageFilter = new NumberRangePopup();
private final BooleanFilterField aliveFilter = new BooleanFilterField();
private final EnumFilterField<Person.MaritalStatus> maritalStatusFilter = new EnumFilterField<>(Person.MaritalStatus.class);
private final DateRangePopup dateOfBirthFilter = new DateRangePopup();
// ...
private void updateFilter() {
    Condition c = Condition.NO_CONDITION;
    c = c.and(idFilter.getValue().asLongInterval().contains(Person.ID));
    if (!nameFilter.isEmpty()) {
        c = c.and(Person.NAME.startsWithIgnoreCase(nameFilter.getValue()));
    }
    c = c.and(ageFilter.getValue().asIntegerInterval().contains(Person.AGE));
    if (!aliveFilter.isEmpty()) {
        c = c.and(Person.ISALIVE.is(aliveFilter.getValue()));
    }
    if (!maritalStatusFilter.isAllOrNothingSelected()) {
        c = c.and(Person.MARITALSTATUS.in(maritalStatusFilter.getValue()));
    }
    // use browser's time zone, obtainable from ExtendedClientDetails
    c = c.and(dateOfBirthFilter.getValue().contains(Person.DATEOFBIRTH, ZoneId.of("UTC")));
    dataProvider.setFilter(c);
}
```

## POJOs

The `EntityDataProvider`, contrary to its name, supports any POJO (Plain Old Java Object).
For example when you want to represent a SQL View you can create a class representing fields of that view,
without marking the class as `Entity`: that way the class only serves for data fetching but
no `save()`/`create()` is supported. You can use POJOs with database tables as well,
for example if the table has no primary key. This kind of use-case is fully supported:

```java
@Table("Person")
public class PersonPOJO implements Serializable {
    private String name;
    private Integer age;
    private LocalDate dateOfBirth;
    // getters, setters, equals and hashCode omitted for brevity

    public static final TableProperty<PersonPOJO, String> NAME = TableProperty.of(PersonPOJO.class, "name");
    public static final TableProperty<PersonPOJO, Integer> AGE = TableProperty.of(PersonPOJO.class, "age");
    public static final TableProperty<PersonPOJO, LocalDate> DATEOFBIRTH = TableProperty.of(PersonPOJO.class, "dateOfBirth");

    public static final DaoOfAny<PersonPOJO> dao = new DaoOfAny<>(PersonPOJO.class);

    public static EntityDataProvider<PersonPOJO> getDataProvider() {
        return new EntityDataProvider(dao);
    }
}

public class PersonPOJOGrid extends Grid<PersonPOJO> {
    public PersonPOJOGrid() {
        final EntityDataProvider<PersonPOJO> dp = PersonPOJO.getDataProvider();
        dp.setFilter(PersonPOJO.AGE.ge(18));
        setDataProvider(dp);
        // columns...
    }
}
```

Because of the `@Table` annotation, `DaoOfAny` will know to load the POJO from the `Person` database table and use
the standard column mapping. You can use `@Nested`, `@ColumnName`, `@JdbiProperty` and others to alter the mapping;
see the [jdbi-orm](https://gitlab.com/mvysny/jdbi-orm) documentation for more details.

## Joins

The `EntityDataProvider` supports the `DaoOfJoin` which in turn supports any POJO and any SQL statement, including joins:

```java
public class JoinOutcome implements Serializable {
    public static final @NotNull Property<Long> PERSON_ID = Person.ID;
    public static final @NotNull Property<String> PERSON_NAME = Person.NAME;
    public static final @NotNull Property<Long> DEPARTMENT_ID = Department.ID;
    public static final @NotNull Property<String> DEPARTMENT_NAME = Department.NAME;

    @ColumnName("id")
    public Long personId;

    @ColumnName("name")
    public String personName;

    @ColumnName("department_id")
    public Long departmentId;

    @ColumnName("department_name")
    public String departmentName;

    public static class MyDao extends DaoOfJoin<JoinOutcome> {

        public MyDao() {
            super(JoinOutcome.class, "select Person.id, Person.name, Department.id as department_id, Department.name as department_name \n" +
                    "FROM Person join mapping_table m on Person.id = m.person_id join Department on m.department_id = Department.id\n");
        }
    }

    @NotNull
    public static final MyDao dao = new MyDao();
}

public class JoinGrid extends Grid<JoinOutcome> {
    public JoinGrid() {
        final EntityDataProvider<JoinOutcome> dp = JoinOutcome.getDataProvider();
        dp.setFilter(JoinOutcome.PERSON_ID.ge(100));
        setDataProvider(dp);
        // columns...
    }
}
```

The DaoOfJoin will run given SQL query, adding any WHERE/ORDER BY/LIMIT/OFFSET clauses as necessary,
to properly support filtering, sorting and paging. Afterwards, the query result is mapped via JDBI
to the bean. You can use `@Nested`, `@ColumnName`, `@JdbiProperty` and others to alter the mapping;
see the [jdbi-orm](https://gitlab.com/mvysny/jdbi-orm) documentation for more details.

## vok-orm support

The `EntityDataProvider` works with [vok-orm](https://github.com/mvysny/vok-orm)
`KEntity` as well:

```kotlin
data class Person(
    override var id: Long? = null,
    var name: String = "",
    var age: Int = -1
) : KEntity<Long> {
    companion object : Dao<Person, Long>(Person::class.java)
}
val <E: KEntity<ID>, ID> Dao<E, ID>.dataProvider: EntityDataProvider<E> get() = EntityDataProvider(this)

val dp = Person.dataProvider
dp.setSortFields(Person::age.desc)
dp.filter = Person::age.exp.eq(25)
```

## Example projects

* [jdbi-orm-vaadin-crud-demo](https://github.com/mvysny/jdbi-orm-vaadin-crud-demo) - a Vaadin CRUD editor, editing jdbi-orm entities
* [vaadin-simple-security-example](https://github.com/mvysny/vaadin-simple-security-example) - uses jdbi-orm to load users from the database and store salted passwords

For more kotlin-based Vaadin example projects see [vok-orm](https://github.com/mvysny/vok-orm).
