dependencies {
    implementation(libs.slf4j.api)
    api(libs.jdbiorm)
    compileOnly(libs.vaadin.core)
    testImplementation(libs.vaadin.core)

    // tests
    testImplementation(kotlin("test"))
    testImplementation(libs.junit.jupiter.engine)
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
    testImplementation(libs.hikaricp)
    testImplementation(libs.slf4j.simple)
    testImplementation(libs.h2) // https://repo1.maven.org/maven2/com/h2database/h2/
    testImplementation(libs.kaributesting)
    // remember this is a Java project :) Kotlin only for tests
    testImplementation(kotlin("stdlib-jdk8"))

    // also test the vok-orm framework, to make sure that DataProviders are compatible with vok-orm
    testImplementation(libs.vokorm)
}

val publishing = ext["publishing"] as (artifactId: String) -> Unit
publishing("jdbi-orm-vaadin")
