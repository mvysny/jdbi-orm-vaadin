package com.gitlab.mvysny.jdbiorm.vaadin.utils;

import com.gitlab.mvysny.jdbiorm.OrderBy;
import com.gitlab.mvysny.jdbiorm.Property;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import org.jetbrains.annotations.NotNull;

/**
 * Vaadin-related utilities.
 */
public class VaadinUtils {
    private VaadinUtils() {}
    /**
     * Converts JDBI-ORM {@link OrderBy} to Vaadin's {@link QuerySortOrder}.
     * @param orderBy comes from JDBI-ORM
     * @return Vaadin counterpart
     */
    @NotNull
    public static QuerySortOrder toQuerySortOrder(@NotNull OrderBy orderBy) {
        return new QuerySortOrder(orderBy.getProperty().toExternalString(), orderBy.getOrder() == OrderBy.Order.ASC ? SortDirection.ASCENDING : SortDirection.DESCENDING);
    }

    /**
     * Converts Vaadin's {@link QuerySortOrder} to JDBI-ORM {@link OrderBy}.
     * @param querySortOrder Vaadin's {@link QuerySortOrder}
     * @return JDBI-ORM {@link OrderBy}
     */
    @NotNull
    public static OrderBy toOrderBy(@NotNull QuerySortOrder querySortOrder) {
        final Property<?> property = Property.fromExternalString(querySortOrder.getSorted());
        return new OrderBy(property, querySortOrder.getDirection() == SortDirection.ASCENDING ? OrderBy.Order.ASC : OrderBy.Order.DESC);
    }
}
