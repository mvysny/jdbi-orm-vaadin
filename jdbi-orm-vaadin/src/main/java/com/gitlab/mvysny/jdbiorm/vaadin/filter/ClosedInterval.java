package com.gitlab.mvysny.jdbiorm.vaadin.filter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Represents a (potentially unbounded) range of values (for example, numbers or characters).
 * <br/>
 * The {@link #getStart()} or {@link #getEndInclusive()} (or both) may be null, to tell that there is no lower or upper boundary.
 * That may not make the interval infinite since some types are limited in value (for example LocalDate
 * has a minimum and a maximum value).
 * @param <T> the type of the items contained in this interval.
 */
public class ClosedInterval<T extends Comparable<? super T>> {
    /**
     * The minimum value in this range.
     */
    @Nullable
    private final T start;
    /**
     * The maximum value still contained in this range (inclusive).
     */
    @Nullable
    private final T endInclusive;

    /**
     * Creates the interval.
     * @param start The minimum value in the range.
     * @param endInclusive The maximum value still contained in this range (inclusive).
     */
    public ClosedInterval(@Nullable T start, @Nullable T endInclusive) {
        this.start = start;
        this.endInclusive = endInclusive;
    }

    /**
     * The minimum value in the range.
     * @return the minimum value contained in this interval.
     */
    @Nullable
    public final T getStart() {
        return start;
    }

    /**
     * The maximum value still contained in this range (inclusive).
     * @return The maximum value still contained in this range (inclusive).
     */
    @Nullable
    public final T getEndInclusive() {
        return endInclusive;
    }

    /**
     * If true then this range accepts any value. True when both <code>start</code> and <code>endInclusive</code> are null.
     * @return true if this interval contains all possible values; false if the interval is bound at least from one side.
     */
    public boolean isUniversalSet() {
        return getStart() == null && getEndInclusive() == null;
    }

    /**
     * Checks whether the specified <code>value</code> belongs to the range.
     * @param value the value to check.
     * @return true if given value is contained in this interval, false if not.
     */
    public boolean contains(@NotNull T value) {
        final T start = getStart();
        final T endInclusive = getEndInclusive();
        final boolean matchesLowerBoundary = start == null || start.compareTo(value) <= 0;
        if (!matchesLowerBoundary) {
            return false;
        }
        final boolean matchesUpperBoundary = endInclusive == null || value.compareTo(endInclusive) <= 0;
        return matchesUpperBoundary;
    }

    /**
     * Checks whether the range is empty.
     * @return true if this interval contains no items; false if it contains 1 or more items.
     */
    public boolean isEmpty() {
        final T start = getStart();
        if (start == null) {
            return false;
        }
        final T end = getEndInclusive();
        if (end == null) {
            return false;
        }
        return start.compareTo(end) > 0;
    }

    /**
     * True if the interval consists of single number only (is degenerate).
     * @return true if the interval contains exactly one item; false if the interval contains no items or if it contains 2 or more items.
     */
    public boolean isSingleItem() {
        return isBound() && getStart().compareTo(getEndInclusive()) == 0;
    }

    /**
     * True if the interval is bound (both {@link #getStart()} and {@link #getEndInclusive()} are not null).
     * @return false if the interval is open (contains infinite amount of numbers).
     */
    public boolean isBound() {
        return getStart() != null && getEndInclusive() != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ClosedInterval<?> that)) return false;
        return Objects.equals(start, that.start) && Objects.equals(endInclusive, that.endInclusive);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, endInclusive);
    }

    @Override
    public String toString() {
        return start + ".." + endInclusive;
    }
}
