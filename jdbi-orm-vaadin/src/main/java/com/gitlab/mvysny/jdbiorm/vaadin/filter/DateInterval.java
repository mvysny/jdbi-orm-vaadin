package com.gitlab.mvysny.jdbiorm.vaadin.filter;

import com.gitlab.mvysny.jdbiorm.Property;
import com.gitlab.mvysny.jdbiorm.condition.Condition;
import com.gitlab.mvysny.jdbiorm.condition.Expression;
import com.gitlab.mvysny.jdbiorm.condition.Op;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * A potentially unbounded date range. If both {@link #getStart()} and {@link #getEndInclusive()} are `null`, then the interval accepts any date.
 * <p>
 * Immutable, thread-safe.
 */
public final class DateInterval extends ClosedInterval<LocalDate> implements Serializable {
    /**
     * Creates the interval.
     * @param start The minimum date in the range.
     * @param endInclusive The maximum date still contained in the range (inclusive).
     */
    public DateInterval(@Nullable LocalDate start, @Nullable LocalDate endInclusive) {
        super(start, endInclusive);
    }

    /**
     * An empty interval which contains no items.
     */
    @NotNull
    public static final DateInterval EMPTY =
            new DateInterval(LocalDate.of(2000, 1, 2), LocalDate.of(2000, 1, 1));
    /**
     * A universal interval which contains all possible dates.
     */
    @NotNull
    public static final DateInterval UNIVERSAL = new DateInterval(null, null);

    /**
     * Produces a degenerate date interval that only contains {@link LocalDate#now(ZoneId)}.
     * @param zoneId the zone ID to use, not null.
     * @return an interval with just one item.
     */
    public static DateInterval now(@NotNull ZoneId zoneId) {
        return of(LocalDate.now(zoneId));
    }

    /**
     * Produces a degenerate date interval that only contains given date.
     * @param localDate the date
     * @return an interval with just one item.
     */
    @NotNull
    public static DateInterval of(@NotNull LocalDate localDate) {
        return new DateInterval(localDate, localDate);
    }

    /**
     * Creates a condition which limits given expression to the values contained in this interval:
     * <pre><code>
     * Condition filter = Condition.NO_CONDITION;
     * filter = filter.and(dateRangePopup.getValue().contains(Person.DATE_OF_BIRTH));
     * </code></pre>
     * Properly handles time-based properties as well: all instants up to the end of the last day of the interval (23:59:59) are accepted.
     * <br/>
     * Ideally you would write <code>expression.between(numberInterval)</code> but Java lacks extension
     * functions, and therefore you need to write <code>numberInterval.contains(expression)</code> instead.
     * @param expression the expression.
     * @param zoneId the zone to use when calculating the end of the day. Usually it's best to use browser's zone ID which
     *               can be retrieved using Vaadin's {@link com.vaadin.flow.component.page.ExtendedClientDetails}.
     * @return the condition, not null. Returns {@link Condition#NO_CONDITION} for the universal set.
     */
    @NotNull
    public Condition contains(@NotNull Property<?> expression, @NotNull ZoneId zoneId) {
        return contains(expression, expression.getValueType(), zoneId);
    }

    @SuppressWarnings("unchecked")
    @NotNull
    Condition contains(@NotNull Expression<?> expression, @NotNull Class<?> valueType, @NotNull ZoneId zoneId) {
        final Condition c1 = compare(((Expression<Object>) expression), valueType, getStart(), Op.Operator.GE, zoneId);
        final Condition c2 = compare(((Expression<Object>) expression), valueType, getEndInclusive(), Op.Operator.LE, zoneId);
        return c1.and(c2);
    }

    /**
     * Creates a condition which compares property against given date:
     * <code>property OP date</code>.
     * @param property the property to compare
     * @param valueType the value type of the property, must be something resembling a date, e.g. {@link LocalDate}, {@link Instant} or such.
     * @param date the date to compare the property against
     * @param operator the comparing operator, e.g. LE
     * @param zoneId zone to use when comparing <code>date</code> against {@link Instant}.
     * @return the condition.
     */
    @NotNull
    private static Condition compare(@NotNull Expression<Object> property, @NotNull Class<?> valueType, @Nullable LocalDate date, @NotNull Op.Operator operator, @NotNull ZoneId zoneId) {
        if (date == null) {
            return Condition.NO_CONDITION;
        }
        if (valueType == LocalDate.class) {
            return property.op(operator, date);
        }
        final LocalDateTime dateTime = operator == Op.Operator.LE || operator == Op.Operator.LT
                ? date.plusDays(1).atStartOfDay().minusSeconds(1)
                : date.atStartOfDay();
        if (valueType == LocalDateTime.class) {
            return property.op(operator, dateTime);
        }
        final Instant instant = dateTime.atZone(zoneId).toInstant();
        if (valueType == Instant.class) {
            return property.op(operator, instant);
        }
        if (Date.class.isAssignableFrom(valueType)) {
            return property.op(operator, new Date(instant.toEpochMilli()));
        }
        if (Calendar.class.isAssignableFrom(valueType)) {
            final Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(instant.toEpochMilli()));
            return property.op(operator, cal);
        }
        throw new IllegalArgumentException("Parameter property " + property + ": invalid value type " + valueType + ": unsupported date type, can not compare");
    }
}
