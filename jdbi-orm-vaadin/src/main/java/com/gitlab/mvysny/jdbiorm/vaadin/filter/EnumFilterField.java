package com.gitlab.mvysny.jdbiorm.vaadin.filter;

import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.combobox.MultiSelectComboBoxVariant;
import org.jetbrains.annotations.NotNull;

/**
 * Allows the user to pick a bunch of enum constants. If no constants are picked, the filter
 * should be ignored. Use {@link #isAllOrNothingSelected()} to detect this.
 * <br/>
 * If the field should allow only a subset of enum constants to be selected, call {@link #setItems(Object[])}
 * to set allowed enum constants.
 * <br/>
 * No enum constants are selected by default.
 * @param <E> the type of enum constants shown in this field.
 */
public class EnumFilterField<E extends Enum<E>> extends MultiSelectComboBox<E> {
    /**
     * Creates the field.
     * @param enumClass the enum class, not null.
     */
    public EnumFilterField(@NotNull Class<E> enumClass) {
        setClearButtonVisible(true);
        addThemeVariants(MultiSelectComboBoxVariant.LUMO_SMALL);
        setItems(enumClass.getEnumConstants());
    }

    /**
     * Returns true if all enum constants are selected.
     * @return true if all enum constants are selected.
     */
    public boolean isAllSelected() {
        return getSelectedItems().size() == getListDataView().getItemCount();
    }

    /**
     * Returns true if either all constants are selected, or none are selected.
     * If this returns false, this filter should be ignored and not applied to the SQL query.
     * @return false if this filter should be ignored since it matches all enum constants.
     */
    public boolean isAllOrNothingSelected() {
        return getSelectedItems().isEmpty() || isAllSelected();
    }
}
