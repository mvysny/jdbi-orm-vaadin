package com.gitlab.mvysny.jdbiorm.vaadin.filter;

import com.gitlab.mvysny.jdbiorm.condition.Expression;
import com.gitlab.mvysny.jdbiorm.vaadin.utils.PopupButton;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.customfield.CustomFieldVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datepicker.DatePickerVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

/**
 * Only shows a single button as its contents. When the button is clicked, it opens a dialog and allows the user to specify a range
 * of dates. When the user sets the values, the dialog is
 * hidden and the number range is set as the value of the popup.
 * <br/>
 * The current date range is also displayed as the caption of the button.
 * <br/>
 * {@link #getValue()} never returns null - by default an {@link ClosedInterval#isUniversalSet() universal set}
 * is returned. Use {@link NumberInterval#contains(Expression)} to easily convert this filter to a SQL statement.
 */
public class DateRangePopup extends CustomField<DateInterval> {
    /**
     * The "From" field, shown in the popup.
     */
    @NotNull
    private final DatePicker fromField = new DatePicker();
    /**
     * The "To" field, shown in the popup.
     */
    @NotNull
    private final DatePicker toField = new DatePicker();
    /**
     * The popup button, shown by this {@link CustomField}.
     */
    @NotNull
    private final PopupButton content = new PopupButton();

    /**
     * Creates the popup.
     */
    public DateRangePopup() {
        super(DateInterval.UNIVERSAL);
        fromField.setId("from");
        fromField.setPlaceholder("From");
        toField.setId("to");
        toField.setPlaceholder("To");
        fromField.addThemeVariants(DatePickerVariant.LUMO_SMALL);
        fromField.setClearButtonVisible(true);
        toField.addThemeVariants(DatePickerVariant.LUMO_SMALL);
        toField.setClearButtonVisible(true);
        content.setPopupContent(new HorizontalLayout(fromField, new Span(".."), toField));
        fromField.addValueChangeListener(e -> {
            updateValue();
            updateCaption();
        });
        toField.addValueChangeListener(e -> {
            updateValue();
            updateCaption();
        });
        updateCaption();
        add(content);
        content.addThemeVariants(MenuBarVariant.LUMO_SMALL);
        addThemeVariants(CustomFieldVariant.LUMO_SMALL);
    }

    /**
     * The "From" field, shown in the popup. You can customize the field to e.g.
     * set a localized caption, or add themes/CSS classes.
     * @return The "From" field, shown in the popup.
     */
    @NotNull
    public DatePicker getFromField() {
        return fromField;
    }

    /**
     * The "To" field, shown in the popup. You can customize the field to e.g.
     * set a localized caption, or add themes/CSS classes.
     * @return The "To" field, shown in the popup.
     */
    @NotNull
    public DatePicker getToField() {
        return toField;
    }

    @Override
    protected DateInterval generateModelValue() {
        return new DateInterval(fromField.getValue(), toField.getValue());
    }

    @Override
    public void setValue(DateInterval value) {
        if (value == null) {
            value = DateInterval.UNIVERSAL;
        }
        super.setValue(value);
    }

    @Override
    protected void setPresentationValue(@Nullable DateInterval newPresentationValue) {
        if (newPresentationValue == null) {
            newPresentationValue = DateInterval.UNIVERSAL;
        }
        fromField.setValue(newPresentationValue.getStart());
        toField.setValue(newPresentationValue.getEndInclusive());
        updateCaption();
    }

    private void updateCaption() {
        final DateInterval value = getValue();
        if (value == null || value.isUniversalSet()) {
            content.setCaption("All");
        } else {
            content.setCaption(format(value.getStart()) + " - " + format(value.getEndInclusive()));
        }
    }

    @NotNull
    private DateTimeFormatter getFormatter() {
        Locale locale = UI.getCurrent().getLocale();
        // use SHORT formatting: if the component is present in grid header cell,
        // the text "Jan 2, 2024 - Jan 16, 2024" may be twice as long as the cell
        // content (date formatted as "Jan 2, 2024").
        return DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
                .withLocale(locale == null ? Locale.getDefault() : locale);
    }

    @NotNull
    private String format(@Nullable LocalDate date) {
        return date == null ? "" : getFormatter().format(date);
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        fromField.setReadOnly(readOnly);
        toField.setReadOnly(readOnly);
    }
}
