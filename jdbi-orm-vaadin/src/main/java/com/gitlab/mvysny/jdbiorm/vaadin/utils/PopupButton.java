package com.gitlab.mvysny.jdbiorm.vaadin.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import org.jetbrains.annotations.NotNull;

/**
 * A popup button: a button with a caption, which, upon clicking, will show a
 * popup with contents.
 * @author mavi@vaadin.com
 */
public class PopupButton extends Composite<Component> implements HasSize {
    /**
     * Internally, the button is implemented via Vaadin {@link MenuBar} with one root {@link #menuItem}.
     */
    @NotNull
    private final MenuBar menu = new MenuBar();
    /**
     * The single root menu item in the {@link #menu}. Holds the contents of the popup;
     * the contents are automatically shown when the menu item is clicked.
     */
    @NotNull
    private final MenuItem menuItem = menu.addItem("");

    /**
     * Returns the current caption shown on the button.
     * @return the current caption, empty by default.
     */
    @NotNull
    public String getCaption() {
        return menuItem.getText();
    }

    /**
     * The caption, shown on the button.
     * @param caption the new caption.
     */
    public void setCaption(@NotNull String caption) {
        menuItem.setText(caption);
    }

    /**
     * Creates a new popup button, with an empty caption.
     */
    public PopupButton() {
    }

    /**
     * Creates a new popup button with given caption.
     * @param caption The caption, shown on the button.
     */
    public PopupButton(@NotNull String caption) {
        this();
        setCaption(caption);
    }

    @Override
    protected Component initContent() {
        return menu;
    }

    /**
     * Sets the popup contents, removing any previously set content.
     * @param content the new content to set, not null.
     */
    public void setPopupContent(@NotNull Component content) {
        menuItem.getSubMenu().removeAll();
        menuItem.getSubMenu().add(content);
    }

    /**
     * Adds theme variants to the component.
     * @param variants theme variants to add
     */
    public void addThemeVariants(MenuBarVariant... variants) {
        menu.addThemeVariants(variants);
    }

    /**
     * Closes the popup button. Does nothing if the popup button is already closed.
     */
    public void close() {
        // workaround for https://github.com/vaadin/vaadin-menu-bar/issues/102
        menu.getElement().executeJs("this._subMenu.close()");
    }
}
