package com.gitlab.mvysny.jdbiorm.vaadin;

import com.gitlab.mvysny.jdbiorm.DaoOfAny;
import com.gitlab.mvysny.jdbiorm.EntityMeta;
import com.gitlab.mvysny.jdbiorm.OrderBy;
import com.gitlab.mvysny.jdbiorm.Property;
import com.gitlab.mvysny.jdbiorm.condition.Condition;
import com.gitlab.mvysny.jdbiorm.vaadin.utils.VaadinUtils;
import com.vaadin.flow.data.provider.*;
import com.vaadin.flow.function.SerializableFunction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Provides instances of given entity {@link T}. Accepts filter of type {@link Condition}.
 * Configurable - call {@link #setFilter(Condition)} to set a permanent filter. The filter
 * is ANDed with {@link Query#getFilter() filter passed through Query}.
 * <br/>
 * Use {@link #withStringFilter(SerializableFunction)} to use this data provider easily from a ComboBox.
 * Use {@link #setSortFields(OrderBy...)} to set the default record ordering.
 * <br/>
 * The {@link QuerySortOrder#getSorted()} passed via Vaadin {@link Query}
 * must be produced by {@link Property#toExternalString()} and parsable via {@link Property#fromExternalString(String)}.
 * {@link QuerySortOrder#getSorted()} is by default produced from <code>Grid.Column.key</code>, but
 * can be changed by calling <code>Grid.Column.setSortProperty()</code> and/or <code>Grid.Column.setSortOrderProvider()</code>.
 * @param <T> the type of the entity returned by this data provider.
 */
public class EntityDataProvider<T> extends AbstractBackEndDataProvider<T, Condition> implements ConfigurableFilterDataProvider<T, Condition, Condition> {
    /**
     * The cached entity metadata.
     */
    @NotNull
    private final EntityMeta<T> entityMeta;
    /**
     * DAO instance for loading entities from the database.
     */
    @NotNull
    private final DaoOfAny<T> dao;
    /**
     * True if the entity {@link T} has an ID property. The value of {@link EntityMeta#hasIdProperty()}.
     */
    private final boolean hasId;

    /**
     * Configurable filter, set via {@link #setFilter(Condition)}.
     */
    @NotNull
    private Condition configuredFilter = Condition.NO_CONDITION;

    /**
     * Creates the data provider, polling data from given DAO. All DAO types are supported, including
     * DaoOfJoin.
     * @param dao the DAO to poll the data from.
     */
    public EntityDataProvider(@NotNull DaoOfAny<T> dao) {
        this.dao = Objects.requireNonNull(dao);
        entityMeta = EntityMeta.of(dao.entityClass);
        hasId = entityMeta.hasIdProperty();
    }

    /**
     * Creates the data provider returning instances of given entity.
     * @param entityClass the type of the entity. Doesn't have to be an actual entity - POJOs
     *                    polling data from a table are supported as well.
     */
    public EntityDataProvider(@NotNull Class<T> entityClass) {
        this(new DaoOfAny<>(entityClass));
    }

    @Override
    public Object getId(T item) {
        if (hasId) {
            return entityMeta.getId(item);
        }
        return super.getId(item);
    }

    /**
     * Calculates the final Condition from {@link #configuredFilter} and from the condition
     * passed from the query.
     * @param query the Vaadin Query, not null.
     * @return the combined condition, never null, may be {@link Condition#NO_CONDITION}.
     */
    @NotNull
    private Condition calculateCondition(@NotNull Query<T, Condition> query) {
        final Condition queryFilter = query.getFilter().orElse(Condition.NO_CONDITION);
        return queryFilter.and(configuredFilter);
    }

    @Override
    protected Stream<T> fetchFromBackEnd(@NotNull Query<@NotNull T, @Nullable Condition> query) {
        final Condition condition = calculateCondition(query);
        final List<OrderBy> order = query.getSortOrders() == null ? Collections.emptyList() :
                query.getSortOrders().stream().map(VaadinUtils::toOrderBy).collect(Collectors.toList());
        final List<T> list = dao.findAllBy(condition, order, (long) query.getOffset(), (long) query.getLimit());
        return list.stream();
    }

    @Override
    protected int sizeInBackEnd(@NotNull Query<@NotNull T, @Nullable Condition> query) {
        final Condition condition = calculateCondition(query);
        return (int) dao.countBy(condition);
    }

    @Override
    public void setFilter(@Nullable Condition filter) {
        filter = filter == null ? Condition.NO_CONDITION : filter;
        if (!Objects.equals(configuredFilter, filter)) {
            this.configuredFilter = filter;
            refreshAll();
        }
    }

    /**
     * Returns the current filter as configured via {@link #setFilter(Condition)}.
     * @return the current filter, defaults to {@link Condition#NO_CONDITION}.
     */
    @Nullable
    public Condition getFilter() {
        return configuredFilter;
    }

    /**
     * Allows this data provider to be set to a Vaadin component which performs String-based
     * filtering, e.g. ComboBox. When the user types in something in
     * hopes to filter the items in the dropdown, <code>filterConverter</code> is invoked, to convert the user input
     * into {@link Condition}.
     *
     * @param filterConverter only invoked when the user types in something. The String is guaranteed to be
     *                        non-null, non-blank and trimmed.
     * @return a new {@link DataProvider}, accepting String filters.
     */
    @NotNull
    public DataProvider<T, String> withStringFilter(@NotNull SerializableFunction<@NotNull String, @Nullable Condition> filterConverter) {
        return withConvertedFilter(filter -> {
            final String postProcessedFilter = filter == null ? "" : filter.trim();
            if (!postProcessedFilter.isEmpty()) {
                return filterConverter.apply(postProcessedFilter);
            } else {
                return null;
            }
        });
    }

    /**
     * Sets a list of sort orders to use as the default sorting for this data
     * provider. This overrides the sorting set by any other method that
     * manipulates the default sorting of this data provider.
     * <p>
     * The default sorting is used if the query defines no sorting. The default
     * sorting is also used to determine the ordering of items that are
     * considered equal by the sorting defined in the query.
     * <p>
     * Example of use: <br><code>setSortFields(Person.ID.desc())</code>
     * @param fields
     *            a list of sort orders to set, not <code>null</code>
     */
    public void setSortFields(@NotNull OrderBy... fields) {
        if (fields == null) {
            setSortFields(Collections.emptyList());
        } else {
            setSortFields(Arrays.asList(fields));
        }
    }

    /**
     * Sets a list of sort orders to use as the default sorting for this data
     * provider. This overrides the sorting set by any other method that
     * manipulates the default sorting of this data provider.
     * <p>
     * The default sorting is used if the query defines no sorting. The default
     * sorting is also used to determine the ordering of items that are
     * considered equal by the sorting defined in the query.
     *
     * @param fields
     *            a list of sort orders to set, not <code>null</code>
     */
    public void setSortFields(@NotNull List<@NotNull OrderBy> fields) {
        setSortOrders(fields.stream().map(VaadinUtils::toQuerySortOrder).collect(Collectors.toList()));
    }
}
