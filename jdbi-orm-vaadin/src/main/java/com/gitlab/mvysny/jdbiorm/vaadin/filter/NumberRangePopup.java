package com.gitlab.mvysny.jdbiorm.vaadin.filter;

import com.gitlab.mvysny.jdbiorm.condition.Expression;
import com.gitlab.mvysny.jdbiorm.vaadin.utils.PopupButton;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.customfield.CustomFieldVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Only shows a single button as its contents. When the button is clicked, it opens a dialog and allows the user to specify a range
 * of numbers. When the user sets the values, the dialog is
 * hidden and the number range is set as the value of the popup.
 * <br/>
 * The current numeric range is also displayed as the caption of the button.
 * <br/>
 * {@link #getValue()} never returns null - by default an {@link ClosedInterval#isUniversalSet() universal set}
 * is returned. Use {@link NumberInterval#contains(Expression)} to easily convert this filter to a SQL statement.
 */
public class NumberRangePopup extends CustomField<NumberInterval<Double>> {
    /**
     * The "From" field, shown in the popup.
     */
    @NotNull
    private final NumberField fromField = new NumberField("", "From");
    /**
     * The "To" field, shown in the popup.
     */
    @NotNull
    private final NumberField toField = new NumberField("", "To");
    /**
     * The popup button, shown by this {@link CustomField}.
     */
    @NotNull
    private final PopupButton content = new PopupButton();

    /**
     * Creates the popup.
     */
    public NumberRangePopup() {
        super(new NumberInterval<>(null, null));
        fromField.setId("from");
        toField.setId("to");
        fromField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        fromField.setClearButtonVisible(true);
        toField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        toField.setClearButtonVisible(true);
        content.setPopupContent(new HorizontalLayout(fromField, new Span(".."), toField));
        fromField.addValueChangeListener(e -> {
            updateValue();
            updateCaption();
        });
        toField.addValueChangeListener(e -> {
            updateValue();
            updateCaption();
        });
        updateCaption();
        add(content);
        content.addThemeVariants(MenuBarVariant.LUMO_SMALL);
        addThemeVariants(CustomFieldVariant.LUMO_SMALL);
    }

    /**
     * The "From" field, shown in the popup. You can customize the field to e.g.
     * set a localized caption, or add themes/CSS classes.
     * @return The "From" field, shown in the popup.
     */
    @NotNull
    public NumberField getFromField() {
        return fromField;
    }

    /**
     * The "To" field, shown in the popup. You can customize the field to e.g.
     * set a localized caption, or add themes/CSS classes.
     * @return The "To" field, shown in the popup.
     */
    @NotNull
    public NumberField getToField() {
        return toField;
    }

    @Override
    protected NumberInterval<Double> generateModelValue() {
        return new NumberInterval<>(fromField.getValue(), toField.getValue());
    }

    @Override
    public void setValue(NumberInterval<Double> value) {
        if (value == null) {
            value = new NumberInterval<>(null, null);
        }
        super.setValue(value);
    }

    @Override
    protected void setPresentationValue(@Nullable NumberInterval<Double> newPresentationValue) {
        if (newPresentationValue == null) {
            newPresentationValue = new NumberInterval<>(null, null);
        }
        fromField.setValue(newPresentationValue.getStart());
        toField.setValue(newPresentationValue.getEndInclusive());
        updateCaption();
    }

    private void updateCaption() {
        final NumberInterval<Double> value = getValue();
        if (value == null || value.isUniversalSet()) {
            content.setCaption("All");
        } else if (value.isSingleItem()) {
            content.setCaption("[x] = " + value.getStart());
        } else if (value.isBound()) {
            content.setCaption(value.getStart() + " ≤ [x] ≤ " + value.getEndInclusive());
        } else if (value.getStart() != null) {
            content.setCaption("[x] ≥ " + value.getStart());
        } else {
            content.setCaption("[x] ≤ " + value.getEndInclusive());
        }
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        fromField.setReadOnly(readOnly);
        toField.setReadOnly(readOnly);
    }
}
