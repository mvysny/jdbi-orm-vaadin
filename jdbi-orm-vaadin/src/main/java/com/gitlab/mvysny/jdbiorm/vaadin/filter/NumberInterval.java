package com.gitlab.mvysny.jdbiorm.vaadin.filter;

import com.gitlab.mvysny.jdbiorm.condition.Condition;
import com.gitlab.mvysny.jdbiorm.condition.Expression;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

/**
 * A potentially open numeric range. If both {@link #getStart()} and {@link #getEndInclusive()} are `null`, then the interval accepts any number.
 * <br/>
 * Immutable, thread-safe.
 * @param <T> the type of the numbers, contained in this interval. Usually {@link Long} or {@link Integer}.
 */
public final class NumberInterval<T extends Number & Comparable<? super T>> extends ClosedInterval<T> implements Serializable {
    /**
     * Creates the interval.
     * @param start The minimum number in the range.
     * @param endInclusive The maximum number still contained in the range (inclusive).
     */
    public NumberInterval(@Nullable T start, @Nullable T endInclusive) {
        super(start, endInclusive);
    }

    /**
     * Converts this interval to an {@link Long} interval. The returned interval contains the same values as the original one.
     * Used to e.g. convert Integer interval to Long.
     * @return the interval containing {@link Long} values.
     */
    @NotNull
    public NumberInterval<Long> asLongInterval() {
        return new NumberInterval<>(getStart() == null ? null : getStart().longValue(), getEndInclusive() == null ? null : getEndInclusive().longValue());
    }

    /**
     * Converts this interval to an {@link Integer} interval. The returned interval contains the same values as the original one.
     * Used to e.g. convert Long interval to Integer.
     * @return the interval containing {@link Integer} values.
     */
    @NotNull
    public NumberInterval<Integer> asIntegerInterval() {
        return new NumberInterval<>(getStart() == null ? null : getStart().intValue(), getEndInclusive() == null ? null : getEndInclusive().intValue());
    }

    /**
     * Creates a numeric range, accepting values from given interval.
     * @param start the start of the interval; the smallest number that's still included in the interval.
     * @param endInclusive the end of the interval; the largest number that's still included in the interval.
     * @return the interval
     */
    @NotNull
    public static NumberInterval<Long> ofLong(@Nullable Long start, @Nullable Long endInclusive) {
        return new NumberInterval<>(start, endInclusive);
    }

    /**
     * Creates a numeric range, accepting values from given interval.
     * @param start the start of the interval; the smallest number that's still included in the interval.
     * @param endInclusive the end of the interval; the largest number that's still included in the interval.
     * @return the interval
     */
    @NotNull
    public static NumberInterval<Integer> ofInt(@Nullable Integer start, @Nullable Integer endInclusive) {
        return new NumberInterval<>(start, endInclusive);
    }

    /**
     * Creates a condition which limits given expression to the values contained in this interval:
     * <pre><code>
     * Condition filter = Condition.NO_CONDITION;
     * filter = filter.and(numberRangePopup.getValue().toCondition(Person.ID));
     * </code></pre>
     * Ideally you would write <code>expression.between(numberInterval)</code> but Java lacks extension
     * functions, and therefore you need to write <code>numberInterval.toCondition(expression)</code> instead.
     * @param expression the expression.
     * @return the condition, not null. Returns {@link Condition#NO_CONDITION} for the universal set.
     */
    @NotNull
    public Condition contains(@NotNull Expression<T> expression) {
        if (isSingleItem()) {
            return expression.eq(getEndInclusive());
        }
        if (!isUniversalSet()) {
            return expression.between(getStart(), getEndInclusive());
        }
        return Condition.NO_CONDITION;
    }
}
