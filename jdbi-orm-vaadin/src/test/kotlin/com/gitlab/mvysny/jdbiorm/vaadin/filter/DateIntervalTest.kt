package com.gitlab.mvysny.jdbiorm.vaadin.filter

import com.gitlab.mvysny.jdbiorm.condition.Expression
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Calendar
import java.util.Date
import kotlin.test.expect

class DateIntervalTest {
    @Test fun empty() {
        expect(true) { DateInterval.EMPTY.isEmpty() }
        expect(false) { DateInterval.UNIVERSAL.isEmpty() }
        expect(false) { DateInterval.of(LocalDate.of(2000, 1, 1)).isEmpty() }
    }

    @Test fun isBound() {
        expect(true) { DateInterval.EMPTY.isBound }
        expect(false) { DateInterval.UNIVERSAL.isBound }
        expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).isBound }
    }

    @Test fun isSingleItem() {
        expect(false) { DateInterval.EMPTY.isSingleItem }
        expect(false) { DateInterval.UNIVERSAL.isSingleItem }
        expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).isSingleItem }
    }

    @Test fun isUniversalSet() {
        expect(false) { DateInterval.EMPTY.isUniversalSet }
        expect(true) { DateInterval.UNIVERSAL.isUniversalSet }
        expect(false) { DateInterval.of(LocalDate.of(2000, 1, 1)).isUniversalSet }
    }

    @Test fun contains() {
        val probe = LocalDate.of(2000, 1, 1)
        expect(false) { DateInterval.EMPTY.contains(probe) }
        expect(false) { DateInterval.EMPTY.contains(DateInterval.EMPTY.endInclusive!!) }
        expect(true) { DateInterval.UNIVERSAL.contains(probe) }
        expect(true) { DateInterval.of(probe).contains(probe) }
        expect(true) { DateInterval(LocalDate.of(1999, 1, 1), null).contains(probe) }
        expect(true) { DateInterval(null, LocalDate.of(2001, 1, 1)).contains(probe) }
        expect(false) { DateInterval(LocalDate.of(2000, 1, 2), null).contains(probe) }
        expect(false) { DateInterval(null, LocalDate.of(2000, 1, 1).minusDays(1)).contains(probe) }
    }

    @Nested inner class Contains {
        @Test fun LocalDate() {
            val probe = Expression.Value(LocalDate.of(2000, 1, 1))
            val utc = ZoneId.of("UTC")
            val c = LocalDate::class.java
            expect(false) { DateInterval.EMPTY.contains(probe, c, utc).test("ignored") }
            expect(false) { DateInterval.EMPTY.contains(DateInterval.EMPTY.endInclusive!!) }
            expect(true) { DateInterval.UNIVERSAL.contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(1999, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2001, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(LocalDate.of(2000, 1, 2), null).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(null, LocalDate.of(2000, 1, 1).minusDays(1)).contains(probe, c, utc).test("ignore") }
        }
        @Test fun LocalDateTime() {
            val probe = Expression.Value(LocalDateTime.of(2000, 1, 1, 1, 0))
            val utc = ZoneId.of("UTC")
            val c = LocalDateTime::class.java
            expect(false) { DateInterval.EMPTY.contains(probe, c, utc).test("ignored") }
            expect(false) { DateInterval.EMPTY.contains(DateInterval.EMPTY.endInclusive!!) }
            expect(true) { DateInterval.UNIVERSAL.contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(1999, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(2000, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2001, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(LocalDate.of(2000, 1, 2), null).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(null, LocalDate.of(2000, 1, 1).minusDays(1)).contains(probe, c, utc).test("ignore") }
        }
        @Test fun Instant() {
            val utc = ZoneId.of("UTC")
            val probe = Expression.Value(LocalDateTime.of(2000, 1, 1, 1, 0).atZone(utc).toInstant())
            val c = Instant::class.java
            expect(false) { DateInterval.EMPTY.contains(probe, c, utc).test("ignored") }
            expect(false) { DateInterval.EMPTY.contains(DateInterval.EMPTY.endInclusive!!) }
            expect(true) { DateInterval.UNIVERSAL.contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(1999, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(2000, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2001, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(LocalDate.of(2000, 1, 2), null).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(null, LocalDate.of(2000, 1, 1).minusDays(1)).contains(probe, c, utc).test("ignore") }
        }
        @Test fun Date() {
            val utc = ZoneId.of("UTC")
            val probe = Expression.Value(Date(LocalDateTime.of(2000, 1, 1, 1, 0).atZone(utc).toInstant().toEpochMilli()))
            val c = Date::class.java
            expect(false) { DateInterval.EMPTY.contains(probe, c, utc).test("ignored") }
            expect(false) { DateInterval.EMPTY.contains(DateInterval.EMPTY.endInclusive!!) }
            expect(true) { DateInterval.UNIVERSAL.contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(1999, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(2000, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2001, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(LocalDate.of(2000, 1, 2), null).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(null, LocalDate.of(2000, 1, 1).minusDays(1)).contains(probe, c, utc).test("ignore") }
        }
        @Test fun Calendar() {
            val utc = ZoneId.of("UTC")
            val probe = Expression.Value(LocalDateTime.of(2000, 1, 1, 1, 0).atZone(utc).toInstant().toCalendar())
            val c = Calendar::class.java
            expect(false) { DateInterval.EMPTY.contains(probe, c, utc).test("ignored") }
            expect(false) { DateInterval.EMPTY.contains(DateInterval.EMPTY.endInclusive!!) }
            expect(true) { DateInterval.UNIVERSAL.contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval.of(LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(1999, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(LocalDate.of(2000, 1, 1), null).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2001, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(true) { DateInterval(null, LocalDate.of(2000, 1, 1)).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(LocalDate.of(2000, 1, 2), null).contains(probe, c, utc).test("ignore") }
            expect(false) { DateInterval(null, LocalDate.of(2000, 1, 1).minusDays(1)).contains(probe, c, utc).test("ignore") }
        }
    }
}

fun Instant.toCalendar(): Calendar = Calendar.getInstance().apply {
    timeInMillis = this@toCalendar.toEpochMilli()
}
