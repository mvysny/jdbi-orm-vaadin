package com.gitlab.mvysny.jdbiorm.vaadin

import com.gitlab.mvysny.jdbiorm.OrderBy
import com.gitlab.mvysny.jdbiorm.TableProperty
import com.gitlab.mvysny.jdbiorm.vaadin.utils.VaadinUtils
import com.vaadin.flow.data.provider.QuerySortOrder
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import kotlin.reflect.KProperty1
import kotlin.test.expect

val OrderBy.vaadin: QuerySortOrder get() = VaadinUtils.toQuerySortOrder(this)

inline val <reified T, V> KProperty1<T, V>.exp: TableProperty<T, V> get() = TableProperty.of(T::class.java, name)
inline val <reified T> KProperty1<T, *>.asc: OrderBy get() = OrderBy(exp, OrderBy.Order.ASC)
inline val <reified T> KProperty1<T, *>.desc: OrderBy get() = OrderBy(exp, OrderBy.Order.DESC)

/**
 * Clones this object by serialization and returns the deserialized clone.
 * @return the clone of this
 */
fun <T : Serializable> T.cloneBySerialization(): T = javaClass.cast(serializeToBytes().deserialize())

inline fun <reified T: Serializable> ByteArray.deserialize(): T? = T::class.java.cast(
    ObjectInputStream(inputStream()).readObject())

/**
 * Serializes the object to a byte array
 * @return the byte array containing this object serialized form.
 */
fun Serializable?.serializeToBytes(): ByteArray = ByteArrayOutputStream().also { ObjectOutputStream(it).writeObject(this) }.toByteArray()

/**
 * Expects that [actual] list of objects matches [expected] list of objects. Fails otherwise.
 */
fun <T> expectList(vararg expected: T, actual: ()->List<T>) {
    expect(expected.toList(), actual)
}
