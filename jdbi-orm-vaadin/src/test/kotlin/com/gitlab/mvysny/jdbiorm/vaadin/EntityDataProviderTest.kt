package com.gitlab.mvysny.jdbiorm.vaadin

import com.github.mvysny.kaributesting.v10.MockVaadin
import com.github.mvysny.kaributesting.v10.getSuggestions
import com.github.mvysny.kaributesting.v10.setUserInput
import com.gitlab.mvysny.jdbiorm.condition.Condition
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.data.provider.Query
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.expect
import kotlin.test.fail

class EntityDataProviderTest : AbstractH2DatabaseTest() {
    @Test fun serializable() {
        Person.dataProvider.cloneBySerialization()
    }

    @Nested inner class EntityTests : AbstractEntityTests()
    @Nested inner class PersonPOJOTests : AbstractPersonPOJOTests()
    @Nested inner class PersonPOJONoIDTest : AbstractPersonPOJONoIDTest()
    @Nested inner class JoinTests : AbstractJoinTests()
    @Nested inner class VokOrmTests : AbstractVokOrmTests()
}

abstract class AbstractPersonPOJONoIDTest {
    @Nested inner class size {
        @Test fun smoke() {
            val dp = PersonPOJONoID.dataProvider
            dp.size(Query())
            dp.size(Query(PersonPOJONoID.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(), null, PersonPOJONoID.AGE.eq(25)))
            dp.size(
                Query(
                    0,
                    100,
                    listOf(PersonPOJONoID.AGE.asc().vaadin),
                    null,
                    PersonPOJONoID.AGE.eq(25)
                )
            )
        }
        @Test fun simple() {
            val dp = PersonPOJONoID.dataProvider
            expect(100) { dp.size(Query()) }
        }
        @Test fun withCondition() {
            val dp = PersonPOJONoID.dataProvider
            expect(1) { dp.size(Query(PersonPOJONoID.AGE.eq(25))) }
            expect(10) { dp.size(Query(PersonPOJONoID.AGE.lt(25))) }
        }
    }

    @Nested inner class fetch {
        @Test fun smoke() {
            val dp = PersonPOJONoID.dataProvider
            dp.fetch(Query())
            dp.fetch(Query(PersonPOJONoID.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(), null, PersonPOJONoID.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(PersonPOJONoID.AGE.asc().vaadin), null, PersonPOJONoID.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = PersonPOJONoID.dataProvider
            expect((15..114).toList()) { dp.fetch(Query()).toList().map { it.age } }
        }
        @Test fun withCondition() {
            val dp = PersonPOJONoID.dataProvider
            expect(listOf(25)) { dp.fetch(Query(PersonPOJONoID.AGE.eq(25))).toList().map { it.age } }
        }
        @Test fun withSorting() {
            val dp = PersonPOJONoID.dataProvider
            expect((114 downTo 15).toList()) {
                val query = Query<PersonPOJONoID, Condition>(
                    0,
                    100,
                    listOf(PersonPOJONoID.AGE.desc().vaadin),
                    null,
                    null
                )
                dp.fetch(query).toList().map { it.age } }
        }
        @Test fun withConditionAndSorting() {
            val dp = PersonPOJONoID.dataProvider
            expect((24 downTo 15).toList()) {
                val query = Query<PersonPOJONoID, Condition>(
                    0,
                    100,
                    listOf(PersonPOJONoID.AGE.desc().vaadin),
                    null,
                    PersonPOJONoID.AGE.lt(25)
                )
                dp.fetch(query).toList().map { it.age } }
        }
        @Test fun `with condition, sorting and paging`() {
            val dp = PersonPOJONoID.dataProvider
            expect((22 downTo 19).toList()) {
                val query = Query<PersonPOJONoID, Condition>(
                    2,
                    4,
                    listOf(PersonPOJONoID.AGE.desc().vaadin),
                    null,
                    PersonPOJONoID.AGE.lt(25)
                )
                dp.fetch(query).toList().map { it.age }
            }
        }
    }
}

/**
 * test arbitrary SQL with DaoOfJoin
 */
abstract class AbstractJoinTests {
    @Nested inner class size {
        @Test fun smoke() {
            val dp = PersonJoin.dataProvider
            dp.size(Query())
            dp.size(Query(PersonJoin.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(), null, PersonJoin.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(PersonJoin.AGE.asc().vaadin), null, PersonJoin.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = PersonJoin.dataProvider
            expect(100) { dp.size(Query()) }
        }
        @Test fun withCondition() {
            val dp = PersonJoin.dataProvider
            expect(1) { dp.size(Query(PersonJoin.ID.eq(25L))) }
            expect(25) { dp.size(Query(PersonJoin.ID.lt(25L))) }
        }
    }

    @Nested inner class fetch {
        @Test fun smoke() {
            val dp = PersonJoin.dataProvider
            dp.fetch(Query())
            dp.fetch(Query(PersonJoin.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(), null, PersonJoin.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(PersonJoin.AGE.asc().vaadin), null, PersonJoin.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = PersonJoin.dataProvider
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun withCondition() {
            val dp = PersonJoin.dataProvider
            expect(listOf(25L)) { dp.fetch(Query(PersonJoin.ID.eq(25L))).toList().map { it.id } }
        }
        @Test fun withSorting() {
            val dp = PersonJoin.dataProvider
            expect((99L downTo 0L).toList()) {
                val query = Query<PersonJoin, Condition>(
                    0,
                    100,
                    listOf(PersonJoin.AGE.desc().vaadin),
                    null,
                    null
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition and sorting`() {
            val dp = PersonJoin.dataProvider
            expect((24L downTo 0L).toList()) {
                val query = Query<PersonJoin, Condition>(
                    0,
                    100,
                    listOf(PersonJoin.AGE.desc().vaadin),
                    null,
                    PersonJoin.ID.lt(25L)
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition, sorting and paging`() {
            val dp = PersonJoin.dataProvider
            expect((22L downTo 19L).toList()) {
                val query = Query<PersonJoin, Condition>(
                    2,
                    4,
                    listOf(PersonJoin.AGE.desc().vaadin),
                    null,
                    PersonJoin.ID.lt(25L)
                )
                dp.fetch(query).toList().map { it.id }
            }
        }
    }

    @Nested inner class withStringFilter {
        @Test fun `null string filter produces null condition automatically`() {
            val dp = PersonJoin.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query(null)).toList().map { it.id } }
        }
        @Test fun `blank string filter produces null condition automatically`() {
            val dp = PersonJoin.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query("   ")).toList().map { it.id } }
        }
        @Test fun name() {
            val dp = PersonJoin.dataProvider.withStringFilter { PersonJoin.NAME.likeIgnoreCase("$it%") }
            expect(listOf(0L)) { dp.fetch(Query("Name 0")).toList().map { it.id } }
        }
        @Nested inner class WithVaadinComboBox {
            @BeforeEach fun setupVaadin() { MockVaadin.setup() }
            @AfterEach fun teardownVaadin() { MockVaadin.tearDown() }
            @Test fun suggestionsPopulatedProperly() {
                val dp = PersonJoin.dataProvider.withStringFilter { PersonJoin.NAME.likeIgnoreCase("$it%") }
                val cb: ComboBox<PersonJoin> = ComboBox<PersonJoin>().apply {
                    setItems(dp)
                    setItemLabelGenerator { it.name }
                }
                expect((0..99).map { "Name $it" }) { cb.getSuggestions() }
                cb.setUserInput("Name 1")
                expectList("Name 1", "Name 10", "Name 11", "Name 12", "Name 13", "Name 14", "Name 15", "Name 16", "Name 17", "Name 18", "Name 19") { cb.getSuggestions() }
            }
        }
    }
    @Nested inner class setFilter {
        @Test fun simple() {
            val dp = PersonJoin.dataProvider
            dp.filter = PersonJoin.ID.eq(25)
            expectList(25) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun `combined with Query filter`() {
            val dp = PersonJoin.dataProvider
            dp.filter = PersonJoin.ID.eq(25)
            expectList() { dp.fetch(Query(PersonJoin.ID.eq(14))).toList().map { it.id } }
        }
    }
    @Nested inner class setSortFields {
        @Test fun `setting empty list clears up any previously set sort fields`() {
            val dp = PersonJoin.dataProvider
            dp.setSortFields(PersonJoin.AGE.desc())
            dp.setSortFields()
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun simple() {
            val dp = PersonJoin.dataProvider
            dp.setSortFields(PersonJoin.AGE.desc())
            expect((99L downTo 0L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
    }
}

abstract class AbstractPersonPOJOTests {
    @Nested inner class size {
        @Test fun smoke() {
            val dp = PersonPOJO.dataProvider
            dp.size(Query())
            dp.size(Query(PersonPOJO.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(), null, PersonPOJO.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(PersonPOJO.AGE.asc().vaadin), null, PersonPOJO.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = PersonPOJO.dataProvider
            expect(100) { dp.size(Query()) }
        }
        @Test fun withCondition() {
            val dp = PersonPOJO.dataProvider
            expect(1) { dp.size(Query(PersonPOJO.ID.eq(25L))) }
            expect(25) { dp.size(Query(PersonPOJO.ID.lt(25L))) }
        }
    }

    @Nested inner class fetch {
        @Test fun smoke() {
            val dp = PersonPOJO.dataProvider
            dp.fetch(Query())
            dp.fetch(Query(PersonPOJO.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(), null, PersonPOJO.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(PersonPOJO.AGE.asc().vaadin), null, PersonPOJO.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = PersonPOJO.dataProvider
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun withCondition() {
            val dp = PersonPOJO.dataProvider
            expect(listOf(25L)) { dp.fetch(Query(PersonPOJO.ID.eq(25L))).toList().map { it.id } }
        }
        @Test fun withSorting() {
            val dp = PersonPOJO.dataProvider
            expect((99L downTo 0L).toList()) {
                val query = Query<PersonPOJO, Condition>(
                    0,
                    100,
                    listOf(PersonPOJO.AGE.desc().vaadin),
                    null,
                    null
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition and sorting`() {
            val dp = PersonPOJO.dataProvider
            expect((24L downTo 0L).toList()) {
                val query = Query<PersonPOJO, Condition>(
                    0,
                    100,
                    listOf(PersonPOJO.AGE.desc().vaadin),
                    null,
                    PersonPOJO.ID.lt(25L)
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition, sorting and paging`() {
            val dp = PersonPOJO.dataProvider
            expect((22L downTo 19L).toList()) {
                val query = Query<PersonPOJO, Condition>(
                    2,
                    4,
                    listOf(PersonPOJO.AGE.desc().vaadin),
                    null,
                    PersonPOJO.ID.lt(25L)
                )
                dp.fetch(query).toList().map { it.id }
            }
        }
    }

    @Nested inner class withStringFilter {
        @Test fun `null string filter produces null condition automatically`() {
            val dp = PersonPOJO.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query(null)).toList().map { it.id } }
        }
        @Test fun `blank string filter produces null condition automatically`() {
            val dp = PersonPOJO.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query("   ")).toList().map { it.id } }
        }
        @Test fun name() {
            val dp = PersonPOJO.dataProvider.withStringFilter { PersonPOJO.NAME.likeIgnoreCase("$it%") }
            expect(listOf(0L)) { dp.fetch(Query("Name 0")).toList().map { it.id } }
        }
        @Nested inner class WithVaadinComboBox {
            @BeforeEach fun setupVaadin() { MockVaadin.setup() }
            @AfterEach fun teardownVaadin() { MockVaadin.tearDown() }
            @Test fun `suggestions populated properly`() {
                val dp = PersonPOJO.dataProvider.withStringFilter { PersonPOJO.NAME.likeIgnoreCase("$it%") }
                val cb: ComboBox<PersonPOJO> = ComboBox<PersonPOJO>().apply {
                    setItems(dp)
                    setItemLabelGenerator { it.name }
                }
                expect((0..99).map { "Name $it" }) { cb.getSuggestions() }
                cb.setUserInput("Name 1")
                expectList("Name 1", "Name 10", "Name 11", "Name 12", "Name 13", "Name 14", "Name 15", "Name 16", "Name 17", "Name 18", "Name 19") { cb.getSuggestions() }
            }
        }
    }
    @Nested inner class setFilter {
        @Test fun simple() {
            val dp = PersonPOJO.dataProvider
            dp.filter = PersonPOJO.ID.eq(25)
            expectList(25) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun `combined with Query filter`() {
            val dp = PersonPOJO.dataProvider
            dp.filter = PersonPOJO.ID.eq(25)
            expectList() { dp.fetch(Query(PersonPOJO.ID.eq(14))).toList().map { it.id } }
        }
    }
    @Nested inner class setSortFields {
        @Test fun `setting empty list clears up any previously set sort fields`() {
            val dp = PersonPOJO.dataProvider
            dp.setSortFields(PersonPOJO.AGE.desc())
            dp.setSortFields()
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun simple() {
            val dp = PersonPOJO.dataProvider
            dp.setSortFields(PersonPOJO.AGE.desc())
            expect((99L downTo 0L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
    }
}

abstract class AbstractEntityTests {
    @Nested inner class size {
        @Test fun smoke() {
            val dp = Person.dataProvider
            dp.size(Query())
            dp.size(Query(Person.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(), null, Person.AGE.eq(25)))
            dp.size(Query(0, 100, listOf(Person.AGE.asc().vaadin), null, Person.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = Person.dataProvider
            expect(100) { dp.size(Query()) }
        }
        @Test fun withCondition() {
            val dp = Person.dataProvider
            expect(1) { dp.size(Query(Person.ID.eq(25L))) }
            expect(25) { dp.size(Query(Person.ID.lt(25L))) }
        }
    }

    @Nested inner class fetch {
        @Test fun smoke() {
            val dp = Person.dataProvider
            dp.fetch(Query())
            dp.fetch(Query(Person.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(), null, Person.AGE.eq(25)))
            dp.fetch(Query(0, 100, listOf(Person.AGE.asc().vaadin), null, Person.AGE.eq(25)))
        }
        @Test fun simple() {
            val dp = Person.dataProvider
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun withCondition() {
            val dp = Person.dataProvider
            expect(listOf(25L)) { dp.fetch(Query(Person.ID.eq(25L))).toList().map { it.id } }
        }
        @Test fun withSorting() {
            val dp = Person.dataProvider
            expect((99L downTo 0L).toList()) {
                val query = Query<Person, Condition>(
                    0,
                    100,
                    listOf(Person.AGE.desc().vaadin),
                    null,
                    null
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition and sorting`() {
            val dp = Person.dataProvider
            expect((24L downTo 0L).toList()) {
                val query = Query<Person, Condition>(
                    0,
                    100,
                    listOf(Person.AGE.desc().vaadin),
                    null,
                    Person.ID.lt(25L)
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition, sorting and paging`() {
            val dp = Person.dataProvider
            expect((22L downTo 19L).toList()) {
                val query = Query<Person, Condition>(
                    2,
                    4,
                    listOf(Person.AGE.desc().vaadin),
                    null,
                    Person.ID.lt(25L)
                )
                dp.fetch(query).toList().map { it.id }
            }
        }
    }

    @Nested inner class withStringFilter {
        @Test fun `null string filter produces null condition automatically`() {
            val dp = Person.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query(null)).toList().map { it.id } }
        }
        @Test fun `blank string filter produces null condition automatically`() {
            val dp = Person.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query("   ")).toList().map { it.id } }
        }
        @Test fun name() {
            val dp = Person.dataProvider.withStringFilter { Person.NAME.likeIgnoreCase("$it%") }
            expect(listOf(0L)) { dp.fetch(Query("Name 0")).toList().map { it.id } }
        }
        @Nested inner class WithVaadinComboBox {
            @BeforeEach fun setupVaadin() { MockVaadin.setup() }
            @AfterEach fun teardownVaadin() { MockVaadin.tearDown() }
            @Test fun suggestionsPopulatedProperly() {
                val dp = Person.dataProvider.withStringFilter { Person.NAME.likeIgnoreCase("$it%") }
                val cb: ComboBox<Person> = ComboBox<Person>().apply {
                    setItems(dp)
                    setItemLabelGenerator { it.name }
                }
                expect((0..99).map { "Name $it" }) { cb.getSuggestions() }
                cb.setUserInput("Name 1")
                expectList("Name 1", "Name 10", "Name 11", "Name 12", "Name 13", "Name 14", "Name 15", "Name 16", "Name 17", "Name 18", "Name 19") { cb.getSuggestions() }
            }
        }
    }
    @Nested inner class setFilter {
        @Test fun simple() {
            val dp = Person.dataProvider
            dp.filter = Person.ID.eq(25)
            expectList(25) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun `combined with Query filter`() {
            val dp = Person.dataProvider
            dp.filter = Person.ID.eq(25)
            expectList() { dp.fetch(Query(Person.ID.eq(14))).toList().map { it.id } }
        }
    }
    @Nested inner class setSortFields {
        @Test fun `setting empty list clears up any previously set sort fields`() {
            val dp = Person.dataProvider
            dp.setSortFields(Person.AGE.desc())
            dp.setSortFields()
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun simple() {
            val dp = Person.dataProvider
            dp.setSortFields(Person.AGE.desc())
            expect((99L downTo 0L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
    }
}

abstract class AbstractVokOrmTests {
    @Nested inner class size {
        @Test fun smoke() {
            val dp = KPerson.dataProvider
            dp.size(Query())
            dp.size(Query(KPerson::age.exp.eq(25)))
            dp.size(Query(0, 100, listOf(), null, KPerson::age.exp.eq(25)))
            dp.size(Query(0, 100, listOf(KPerson::age.asc.vaadin), null, KPerson::age.exp.eq(25)))
        }
        @Test fun simple() {
            val dp = KPerson.dataProvider
            expect(100) { dp.size(Query()) }
        }
        @Test fun withCondition() {
            val dp = KPerson.dataProvider
            expect(1) { dp.size(Query(KPerson::id.exp.eq(25L))) }
            expect(25) { dp.size(Query(KPerson::id.exp.lt(25L))) }
        }
    }

    @Nested inner class fetch {
        @Test fun smoke() {
            val dp = KPerson.dataProvider
            dp.fetch(Query())
            dp.fetch(Query(KPerson::age.exp.eq(25)))
            dp.fetch(Query(0, 100, listOf(), null, KPerson::age.exp.eq(25)))
            dp.fetch(Query(0, 100, listOf(KPerson::age.asc.vaadin), null, KPerson::age.exp.eq(25)))
        }
        @Test fun simple() {
            val dp = KPerson.dataProvider
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun withCondition() {
            val dp = KPerson.dataProvider
            expect(listOf(25L)) { dp.fetch(Query(KPerson::id.exp.eq(25L))).toList().map { it.id } }
        }
        @Test fun withSorting() {
            val dp = KPerson.dataProvider
            expect((99L downTo 0L).toList()) {
                val query = Query<KPerson, Condition>(
                    0,
                    100,
                    listOf(KPerson::age.desc.vaadin),
                    null,
                    null
                )
                dp.fetch(query).toList().map { it.id }
            }
        }
        @Test fun `with condition and sorting`() {
            val dp = KPerson.dataProvider
            expect((24L downTo 0L).toList()) {
                val query = Query<KPerson, Condition>(
                    0,
                    100,
                    listOf(KPerson::age.desc.vaadin),
                    null,
                    KPerson::id.exp.lt(25L)
                )
                dp.fetch(query).toList().map { it.id } }
        }
        @Test fun `with condition, sorting and paging`() {
            val dp = KPerson.dataProvider
            expect((22L downTo 19L).toList()) {
                val query = Query<KPerson, Condition>(
                    2,
                    4,
                    listOf(KPerson::age.desc.vaadin),
                    null,
                    KPerson::id.exp.lt(25L)
                )
                dp.fetch(query).toList().map { it.id } }
        }
    }

    @Nested inner class withStringFilter {
        @Test fun `null string filter produces null condition automatically`() {
            val dp = KPerson.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query(null)).toList().map { it.id } }
        }
        @Test fun `blank string filter produces null condition automatically`() {
            val dp = KPerson.dataProvider.withStringFilter { fail("shouldn't be called") }
            expect((0L..99L).toList()) { dp.fetch(Query("   ")).toList().map { it.id } }
        }
        @Test fun name() {
            val dp = KPerson.dataProvider.withStringFilter { KPerson::name.exp.likeIgnoreCase("$it%") }
            expect(listOf(0L)) { dp.fetch(Query("Name 0")).toList().map { it.id } }
        }
        @Nested inner class WithVaadinComboBox {
            @BeforeEach fun setupVaadin() { MockVaadin.setup() }
            @AfterEach fun teardownVaadin() { MockVaadin.tearDown() }
            @Test fun suggestionsPopulatedProperly() {
                val dp = KPerson.dataProvider.withStringFilter { KPerson::name.exp.likeIgnoreCase("$it%") }
                val cb: ComboBox<KPerson> = ComboBox<KPerson>().apply {
                    setItems(dp)
                    setItemLabelGenerator { it.name }
                }
                expect((0..99).map { "Name $it" }) { cb.getSuggestions() }
                cb.setUserInput("Name 1")
                expectList("Name 1", "Name 10", "Name 11", "Name 12", "Name 13", "Name 14", "Name 15", "Name 16", "Name 17", "Name 18", "Name 19") { cb.getSuggestions() }
            }
        }
    }
    @Nested inner class setFilter {
        @Test fun simple() {
            val dp = KPerson.dataProvider
            dp.filter = KPerson::id.exp.eq(25)
            expectList(25) { dp.fetch(Query()).toList().map { it.id } }
            expectList() { dp.fetch(Query(KPerson::id.exp.eq(14))).toList().map { it.id } }
        }
    }
    @Nested inner class setSortFields {
        @Test fun `setting empty list clears up any previously set sort fields`() {
            val dp = KPerson.dataProvider
            dp.setSortFields(KPerson::age.desc)
            dp.setSortFields()
            expect((0L..99L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
        @Test fun simple() {
            val dp = KPerson.dataProvider
            dp.setSortFields(KPerson::age.desc)
            expect((99L downTo 0L).toList()) { dp.fetch(Query()).toList().map { it.id } }
        }
    }
}
