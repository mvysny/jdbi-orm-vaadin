package com.gitlab.mvysny.jdbiorm.vaadin

import com.gitlab.mvysny.jdbiorm.JdbiOrm
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.intellij.lang.annotations.Language
import org.jdbi.v3.core.Handle
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.TestInstance
import java.time.LocalDate

/**
 * Sets up connection to an in-memory H2 database.
 * Also creates 100 testing Person instances, with ages 15, 16, ... 114, and IDs of 0, 1, .. 99.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class AbstractH2DatabaseTest {
    private fun hikari(block: HikariConfig.() -> Unit) {
        JdbiOrm.databaseVariant = null
        JdbiOrm.setDataSource(HikariDataSource(HikariConfig().apply(block)))
    }

    private fun Handle.ddl(@Language("sql") sql: String) {
        createUpdate(sql).execute()
    }

    private fun <T> db(block: Handle.() -> T): T = JdbiOrm.jdbi().inTransaction<T, Exception>(block)

    @BeforeAll fun setup() {
        hikari {
            jdbcUrl = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1"
            username = "sa"
            password = ""
        }
        db {
            ddl("DROP ALL OBJECTS")
            ddl("""create table Person (
                id bigint primary key auto_increment,
                name varchar not null,
                age integer not null,
                dateOfBirth date,
                alive boolean,
                maritalStatus varchar
                 )""")
            (0..99).forEach {
                Person(it.toLong(), "Name $it", it + 15, LocalDate.now().minusDays(100L).plusDays(it.toLong()), it % 2 == 0).create()
            }
        }
    }
    @AfterAll
    fun teardown() {
        db { ddl("DROP ALL OBJECTS") }
        JdbiOrm.destroy()
    }
}
