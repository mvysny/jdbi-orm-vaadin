package com.gitlab.mvysny.jdbiorm.vaadin

import com.gitlab.mvysny.jdbiorm.DaoOfAny
import com.gitlab.mvysny.jdbiorm.DaoOfJoin
import com.gitlab.mvysny.jdbiorm.Table
import com.gitlab.mvysny.jdbiorm.TableProperty
import java.time.LocalDate

/**
 * A test POJO which fetches data using [DaoOfAny]. Typically you use POJOs with SQL views,
 * but you can use POJOs with database tables as well, for example if the table has no primary key.
 */
@Table("Person")
data class PersonPOJO(
    var id: Long? = null,
    var name: String = "",
    var age: Int = -1,
    var dateOfBirth: LocalDate? = null,
) {
    companion object : DaoOfAny<PersonPOJO>(PersonPOJO::class.java) {
        @JvmStatic
        val dataProvider: EntityDataProvider<PersonPOJO> get() = EntityDataProvider(this)
        @JvmStatic
        val ID = TableProperty.of<PersonPOJO, Long>(PersonPOJO::class.java, "id")
        @JvmStatic
        val NAME = TableProperty.of<PersonPOJO, String>(PersonPOJO::class.java, "name")
        @JvmStatic
        val AGE = TableProperty.of<PersonPOJO, Int>(PersonPOJO::class.java, "age")
        @JvmStatic
        val DATEOFBIRTH = TableProperty.of<PersonPOJO, LocalDate>(PersonPOJO::class.java, "dateOfBirth")
    }
}

@Table("Person")
data class PersonPOJONoID(
    var name: String = "",
    var age: Int = -1,
    var dateOfBirth: LocalDate? = null,
) {
    companion object : DaoOfAny<PersonPOJONoID>(PersonPOJONoID::class.java) {
        @JvmStatic
        val dataProvider: EntityDataProvider<PersonPOJONoID> get() = EntityDataProvider(this)
        @JvmStatic
        val NAME = TableProperty.of<PersonPOJONoID, String>(PersonPOJONoID::class.java, "name")
        @JvmStatic
        val AGE = TableProperty.of<PersonPOJONoID, Int>(PersonPOJONoID::class.java, "age")
        @JvmStatic
        val DATEOFBIRTH = TableProperty.of<PersonPOJONoID, LocalDate>(PersonPOJONoID::class.java, "dateOfBirth")
    }
}

/**
 * A test POJO which fetches data using [DaoOfJoin]. Typically you would fetch an outcome of
 * join but this is enough for the testing purposes.
 *
 * Note the missing [Table] annotation:
 * if we would to fetch this using [DaoOfAny] it would fail since there's no `PersonJoin` table/view.
 * However, with [DaoOfJoin] we write our own SELECT clauses, and thus this works.
 */
data class PersonJoin(
    var id: Long? = null,
    var name: String = "",
    var age: Int = -1,
    var dateOfBirth: LocalDate? = null,
) {
    companion object : DaoOfJoin<PersonJoin>(PersonJoin::class.java, "SELECT id, name, age, dateOfBirth FROM Person") {
        @JvmStatic
        val dataProvider: EntityDataProvider<PersonJoin> get() = EntityDataProvider(this)
        @JvmStatic
        val ID = Person.ID
        @JvmStatic
        val NAME = Person.NAME
        @JvmStatic
        val AGE = Person.AGE
        @JvmStatic
        val DATEOFBIRTH = Person.DATEOFBIRTH
    }
}
