package testapp

import com.github.mvysny.kaributesting.v10.*
import com.github.mvysny.kaributools.navigateTo
import com.gitlab.mvysny.jdbiorm.vaadin.filter.BooleanFilterField
import com.gitlab.mvysny.jdbiorm.vaadin.filter.FilterTextField
import com.vaadin.flow.component.grid.Grid
import com.vaadin.flow.data.provider.SortDirection
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.test.expect

class MainViewTest {
    companion object {
        private lateinit var routes: Routes
        @BeforeAll @JvmStatic fun setupApp() {
            routes = Routes().autoDiscoverViews("testapp")
            Bootstrap().contextInitialized(null)
        }
        @AfterAll @JvmStatic fun teardownApp() { Bootstrap().contextDestroyed(null) }
    }
    @BeforeEach fun setupVaadin() { MockVaadin.setup(routes) }
    @AfterEach fun teardownVaadin() { MockVaadin.tearDown() }
    @BeforeEach @AfterEach fun clearDb() { Person.dao.deleteAll() }

    @Test fun smoke() {
        navigateTo<MainView>()
        _expectOne<MainView>()
    }

    @Test fun GridInitialContents() {
        Bootstrap.generateTestingData()
        val grid: Grid<Person> = _get<Grid<Person>>()
        grid.expectRows(200)
    }

    @Test fun filtering() {
        Person.createDummy(0)
        val grid = _get<Grid<Person>>()

        val nameFilter = _get<FilterTextField> { id = "nameFilter" }
        nameFilter._value = "foo"
        grid.expectRows(0)
        nameFilter._value = "Jon Lord"
        grid.expectRows(1)
        nameFilter._value = ""
        grid.expectRows(1)

        val aliveFilter = _get<BooleanFilterField> { id = "aliveFilter" }
        aliveFilter._value = true
        grid.expectRows(1)
        aliveFilter._value = false
        grid.expectRows(0)
        aliveFilter._value = true
        nameFilter._value = "foo"
        grid.expectRows(0)
    }

    @Test fun sorting() {
        Bootstrap.generateTestingData()
        val grid = _get<Grid<Person>>()
        grid._sortByKey(Person.ID.toExternalString(), SortDirection.DESCENDING)
        expect((114 downTo 15).toList()) { grid._findAll().map { it.age } .distinct() }

        // smoke tests, to make sure sorting doesn't blow up
        grid._sortByKey(Person.NAME.toExternalString(), SortDirection.DESCENDING)
        grid._findAll()
        grid._sortByKey(Person.AGE.toExternalString(), SortDirection.DESCENDING)
        grid._findAll()
        grid._sortByKey(Person.ISALIVE.toExternalString(), SortDirection.DESCENDING)
        grid._findAll()
        grid._sortByKey(Person.DATEOFBIRTH.toExternalString(), SortDirection.DESCENDING)
        grid._findAll()
        grid._sortByKey(Person.MARITALSTATUS.toExternalString(), SortDirection.DESCENDING)
        grid._findAll()
    }
}
